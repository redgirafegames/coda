﻿using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEditor.ShortcutManagement;
using UnityEngine;

namespace RedGirafeGames.Coda.Editor
{
    public static class Tools
    {
        private const string LogsEnabledId = "Coda.logs.enabled";

        private static bool LogsEnabled
        {
            get => EditorPrefs.HasKey(LogsEnabledId) && EditorPrefs.GetBool(LogsEnabledId);
            set => EditorPrefs.SetBool(LogsEnabledId, value);
        }
        
        [MenuItem("Assets/Coda/Init Project Folder")]
        public static void InitProjectFolder()
        {
            if (Selection.activeObject == null)
                return;
            var path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (!Directory.Exists(path))
                return;

            path += "/";
            GenerateDirectory(path  + "Animation");
            GenerateDirectory(path  + "Audio");
            GenerateDirectory(path  + "Editor");
            GenerateDirectory(path  + "Effects");
            GenerateDirectory(path  + "Materials");
            GenerateDirectory(path  + "Models");
            GenerateDirectory(path  + "Prefabs");
            GenerateDirectory(path  + "Resources");
            GenerateDirectory(path  + "Scenes");
            GenerateDirectory(path  + "ScriptableObjects");
            GenerateDirectory(path  + "Scripts");
            GenerateDirectory(path  + "Shaders");
        }

        private static void GenerateDirectory(string dir)
        {
            Debug.Log("[Coda] Generating Dir : " + dir);
            var infos = Directory.CreateDirectory(dir);
            if (!infos.Exists)
            {
                Debug.Log("[Coda] Generation failed");
            }
        }

        [MenuItem("Coda/Clear Console &c")] // alt + shift + L
        public static void ClearConsole()
        {
            var assembly = Assembly.GetAssembly(typeof(SceneView));
            var type = assembly.GetType("UnityEditor.LogEntries");
            var method = type.GetMethod("Clear");
            method?.Invoke(new object(), null);
        }
        
        [MenuItem("Coda/Toggle Logs")]
        private static void ToggleLogs()
        {
            LogsEnabled = !LogsEnabled;
            Debug.Log("[Coda] Logs " + (LogsEnabled ? "enabled" : "disabled"));
        }

        [MenuItem("Coda/Toggle Inspector Lock &#l")] // alt + shift + L
        private static void ToggleInspectorLock()
        {
            if (Selection.activeGameObject == null)
                return;
            
            var locked = ActiveEditorTracker.sharedTracker.isLocked;
            if(LogsEnabled) Debug.Log("[Coda] " + (locked ? "Unlocking editor" : "Locking editor for" + " : " + Selection.activeGameObject));
            ActiveEditorTracker.sharedTracker.isLocked = !locked;
            ActiveEditorTracker.sharedTracker.ForceRebuild();
            
        }

        [MenuItem("Coda/Reset Local Position &#t")] // alt + shift + t
        private static void ResetLocalPosition()
        {
            if (Selection.activeTransform == null) return;
            
            var selectedGameObjects = Selection.gameObjects;
            
            foreach (var gameObject in selectedGameObjects)
            {
                if(LogsEnabled) Debug.Log("[Coda] Reset local position on : " + gameObject);
                Undo.RecordObject(gameObject.transform, "Reset Local position");
                gameObject.transform.localPosition = Vector3.zero;
            }
        }
        
        [MenuItem("Coda/Reset Global Position &#%t")] // alt + shift + ctrl + t
        private static void ResetGlobalPosition()
        {
            if (Selection.activeTransform == null) return;
            
            var selectedGameObjects = Selection.gameObjects;
            
            foreach (var gameObject in selectedGameObjects)
            {
                if(LogsEnabled) Debug.Log("[Coda] Reset global position on : " + gameObject);
                Undo.RecordObject(gameObject.transform, "Reset Global position");
                gameObject.transform.position = Vector3.zero;
            }
        }
        
        [MenuItem("Coda/Reset Local Rotation &#r")] // alt + shift + r
        private static void ResetLocalRotation()
        {
            if (Selection.activeTransform == null) return;
            
            var selectedGameObjects = Selection.gameObjects;
            
            foreach (var gameObject in selectedGameObjects)
            {
                if(LogsEnabled) Debug.Log("[Coda] Reset local rotation on : " + gameObject);
                Undo.RecordObject(gameObject.transform, "Reset Local rotation");
                gameObject.transform.localRotation = Quaternion.identity;
            }
        }
        
        [MenuItem("Coda/Reset Global Rotation &#%r")] // alt + shift + ctr + r
        private static void ResetGlobalRotation()
        {
            if (Selection.activeTransform == null) return;
            
            var selectedGameObjects = Selection.gameObjects;
            
            foreach (var gameObject in selectedGameObjects)
            {
                if(LogsEnabled) Debug.Log("[Coda] Reset global rotation on : " + gameObject);
                Undo.RecordObject(gameObject.transform, "Reset Global rotation");
                gameObject.transform.rotation = Quaternion.identity;
            }
        }
        
        [MenuItem("Coda/Reset Scale &#s")] // alt + shift + s
        private static void ResetScale()
        {
            if (Selection.activeTransform == null) return;
            
            var selectedGameObjects = Selection.gameObjects;
            
            foreach (var gameObject in selectedGameObjects)
            {
                if(LogsEnabled) Debug.Log("[Coda] Reset scale on : " + gameObject);
                Undo.RecordObject(gameObject.transform, "Reset Scale rotation");
                gameObject.transform.localScale = Vector3.one;
            }
        }
        
        [MenuItem("Coda/Selection distance &#%d")] // alt + shift + ctr + d
        private static void SelectionDistance()
        {
            if (Selection.activeTransform == null)
            {
                Debug.Log("[Coda] Select more than one GameObject to compute distance");
                return;
            }
            
            var selectedGameObjects = Selection.gameObjects;
            if(selectedGameObjects.Length == 1)
            {
                Debug.Log("[Coda] Select more than one GameObject to compute distance");
                return;
            }

            for (var i = 0; i < selectedGameObjects.Length; i++)
            {
                PrintDistanceForGameObject(selectedGameObjects, i);
            }
        }

        private static void PrintDistanceForGameObject(GameObject[] selectedGameObjects, int index)
        {
            var refGameObject = selectedGameObjects[index];
            for (var i = index + 1; i < selectedGameObjects.Length; i++)
            {
                var selected = selectedGameObjects[i];
                var distance = Vector3.Distance(refGameObject.transform.position, selected.transform.position);
                Debug.Log("[Code] Distance <" + refGameObject.name + "> to <" + selected.name + "> = " + distance);
            }
        }
    }
}
